# Project brief
## Theme
University project aimed at **designing and developing a website** describing the concept of an imaginary urban reality.
By urban reality we mean a medium/large sized population centre anywhere.

## Content
The result will be a **responsive site** that develops a **main task and secondary tasks** functional to the first one.
The site must be complete with content and developed from a **mobile first approach in a human-centred perspective**.
The project must also be accompanied by the analysis and research materials and the design system that enables its subsequent development and maintenance.

## Result
The project should pay particular attention to the visual, communicative, interaction and design aspects of the user interface.
The site should have optimal usability on different devices/contexts of use. In particular for:
- **smartphones** (320x568 px);
- **tablet** (portrait orientation 768x1024 px);
- **desktop** (1920x1080 px);

## Features and guidelines
The project must be carried out by a group of 5/6 people.
The aim is to focus the **design attention on aspects of communication, interaction and user interface design**:
- conept;
- information architecture;
- navigation and interaction systems;
- graphic-communication and branding aspects;
- grid and organisation of the interface interaction space;
- typographic, chromatic and iconographic choices;
- usability in mobility;
- content editing and editing;